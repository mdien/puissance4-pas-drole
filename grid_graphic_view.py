# coding: utf-8

import pygame, sys
from pygame.locals import *
from colors import *

def first_empty_line(grid, col):
    for j in range(grid.h):
        if grid[col][j] is None:
            return j


class GridGraphicView(object):

    def __init__(self, grid, p1, p2, screen, cell_size, parent):
        self.screen = screen
        self.p = dict()
        self.p[p1] = RED
        self.p[p2] = YELLOW
        self.p[None] = WHITE
        self.screen = screen
        self.cell_size = cell_size
        self.w = self.screen.get_width()
        self.h = self.screen.get_height()
        self.grid = grid
        self.parent = parent
    
    def draw(self):
        self.screen.fill(WHITE)
        self.draw_grid()
        self.draw_tokens()
        
    def draw_grid(self):

        new_surf = pygame.Surface(self.screen.get_size())
        new_surf.fill(WHITE)
        pygame.draw.rect(new_surf, BLUE, (10,10,self.w-20,self.h-20))

        x,y = (10 + self.cell_size//2), (self.h-10-self.cell_size//2)
        
        for i in range(self.grid.w):
            for j in range(self.grid.h):
                pygame.draw.circle(new_surf, WHITE,
                                   (x+i*self.cell_size, y-j*self.cell_size),
                                   self.cell_size//2-4)
        new_surf.set_colorkey(WHITE)
        self.screen.blit(new_surf, (0,0))


    def draw_tokens(self):
        new_surf = pygame.Surface(self.screen.get_size())
        new_surf.fill(WHITE)

        
        x,y = (10 + self.cell_size//2), (self.h-10-self.cell_size//2)
        
        for i in range(self.grid.w):
            for j in range(self.grid.h):
                pygame.draw.circle(new_surf, self.p[self.grid[i][j]],
                                   (x+i*self.cell_size, y-j*self.cell_size),
                                   self.cell_size//2-4)
        new_surf.set_colorkey(WHITE)
        self.screen.blit(new_surf, (0,0))


                
    def get_col(self):
        pygame.event.clear()

        while True:
            e = pygame.event.wait()
            if e.type == MOUSEBUTTONUP:
                mouse_x, mouse_y = e.pos
                if mouse_x < 10 or e.button != 1:
                    continue
                else:
                    col = (mouse_x - 10) // self.cell_size
                    return col
            if e.type == QUIT:
                sys.exit()

    def anim_fall(self, col, player):
        clock = pygame.time.Clock()

        dest_line = first_empty_line(self.grid, col)
        padx,pady = (10 + self.cell_size//2), (self.h-10-self.cell_size//2)
        x,y = padx + col*self.cell_size, 0

        while y < pady - dest_line * self.cell_size:
            self.screen.fill(WHITE)
            
            pygame.draw.circle(self.screen, self.p[player],
                                   (padx+col*self.cell_size, y),
                                   self.cell_size//2-4)
            self.draw_grid()
            self.draw_tokens()
            
            y += int(self.cell_size*0.1)
            self.parent.update()
            clock.tick(60)
        
