# coding: utf-8

class Grid(object):
    
    def __init__(self, width, height):
        self.w = width
        self.h = height
        self.tab = [[None for _ in range(height)] for _ in range(width)]

    def is_over(self):
        if self.winner():
            return True
        
        for col in self.tab:
            if None in col:
                return False

        return True

    def check_dir(self, o, v):
        x,y = o
        vx,vy = v
        return (x >= 0 and x < self.w and y >= 0 and y < self.h and
                x+3*vx >= 0 and x+3*vx < self.w and
                y+3*vy >= 0 and y+3*vy < self.h)

    
    def check4(self, o, v):
        x, y = o
        vx, vy = v
        
        collect = self.tab[x][y]

        if not collect:
            return None
        
        for i in range(4):
            if collect != self.tab[x+i*vx][y+i*vy]:
                return None

        return collect
        
    def winner(self):

        vs = [(0,1), (1,0), (1,1), (1,-1)]
        
        for i in range(self.w):
            for j in range(self.h):
                for v in vs:
                    if self.check_dir((i,j), v):
                        w = self.check4((i,j), v)
                        if w is not None:
                            return w
        return None
    
    def cell(self, i, j):
        return self.tab[i][j]

    def playable(self, i):
        if i < 0 or i >= self.w:
            return False
        return None in self.tab[i]

    def play(self, i, player):
        for j in range(self.h):
            if self.tab[i][j] is None:
                self.tab[i][j] = player
                return

    def __getitem__(self, i):
        return self.tab[i]
