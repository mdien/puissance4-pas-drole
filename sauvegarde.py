from grid import Grid

def save(filename, game):
    with open(filename,"w") as f:
        f.write(game.p1+"\n")
        f.write(game.p2+"\n")
        f.write(game.cp+"\n")
        f.write("{} {}\n".format(game.grid.w, game.grid.h))
        for i in range(game.grid.w):
            for j in range(game.grid.h):
                if game.grid[i][j] is not None:
                    f.write("{} {}\n".format(i, game.grid[i][j]))

def load(filename):
    with open(filename,"r") as f:
        p1 = f.readline()[:-1]
        p2 = f.readline()[:-1]
        cp = f.readline()[:-1]
        w, h = f.readline()[:-1].split()
        grid = Grid(int(w),int(h))
        for line in f:
            i, player = line[:-1].split()
            grid.play(int(i), player)

        game = (p1, p2, grid, cp)
        return game
