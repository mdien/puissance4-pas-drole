# coding: utf-8

import pygame
import sys
from pygame.locals import *
from game_graphic import GameGraphic
from grid import Grid
import sauvegarde
from colors import *
import tkinter as tk


def make_button(size, text, background=WHITE):
    (w,h) = size
    font = pygame.font.SysFont("monospace", 40)
    text_surf = font.render(text, True, BLACK, background)
    surf = pygame.Surface((w,h))
    surf.fill(background)
    tw,th = text_surf.get_size()
    text_surf = pygame.transform.scale(text_surf, (min(w,tw), min(h,th)))
    surf.blit(text_surf, ((w-min(w,tw))//2,(h-min(h,th))//2))
    return surf
    

def make_menu(screen, items, callbacks):
    assert(len(items) == len(callbacks))
    n = len(items)
    w,h = screen.get_size()
    bw = w // 4
    bh = h // (n+2)
    bpad = bh //(n+1)
    for i in range(n):
        button = make_button((2*bw,bh), items[i])
        screen.blit(button, (bw, bpad*(i+1)+bh*i))

    pygame.display.flip()
        
    pygame.event.clear()
    while True:
        e = pygame.event.wait()
        if e.type == MOUSEBUTTONUP:
            mouse_x, mouse_y = e.pos
            
            if (mouse_x < bw or mouse_x > 3*bw or e.button != 1
                or mouse_y < bpad or mouse_y > n*(bpad+bh)):
                continue
            else:
                i = mouse_y // (bpad+bh)
                if mouse_y % (bpad+bh) > bpad:
                    callbacks[i](screen)
        if e.type == QUIT:
            sys.exit()

def new_game(screen):
    root = tk.Tk()
    frame = tk.Frame(root)
    lblb_p1 = tk.Label(frame, text="Nom du joueur 1 ?")
    lblb_p2 = tk.Label(frame, text="Nom du joueur 2 ?")
    p1 = tk.StringVar()
    p2 = tk.StringVar()
    p1.set("joueur 1")
    p2.set("joueur 2")
    entry_p1 = tk.Entry(frame, textvariable=p1)
    entry_p2 = tk.Entry(frame, textvariable=p2)

    def destroy():
        if p1.get() != "" and p2.get() != "":
            root.destroy()
        else:
            pass

    button = tk.Button(frame, text="Entrer", command=destroy)

    frame.pack()
    lblb_p1.pack()
    entry_p1.pack()
    lblb_p2.pack()
    entry_p2.pack()
    button.pack()
    frame.mainloop()
    
    p1 = p1.get()
    p2 = p2.get()
    
    grid = Grid(7,6)
    game = GameGraphic(screen, 60, p1, p2, grid)
    game.run()

def load(screen):
    game = GameGraphic(screen, 60, *sauvegarde.load("save.p4"))
    game.run()
    
if __name__ == '__main__':

    pygame.init()
    pygame.event.set_allowed(None)
    pygame.event.set_allowed([MOUSEBUTTONUP, QUIT])

    cell_size = 60
    w = 20+7*cell_size
    h = (2+6)*cell_size
    screen = pygame.display.set_mode((w,h))
    pygame.display.set_caption("Puissance 4, mais sans le fun")

    load_cb = lambda screen: load(screen)
    new_game_cb = lambda screen: new_game(screen)
    quit_cb = lambda _: sys.exit()
    callbacks = [new_game_cb, load_cb, quit_cb]
    
    make_menu(screen, ["Jouer", "Charger", "Quitter"], callbacks)
