# coding: utf-8

from grid_graphic_view import GridGraphicView
from sauvegarde import save
from colors import *

import pygame

class GameGraphic(object):

    def __init__(self, screen, cell_size, p1, p2, grid, cp=None):
        self.p1 = p1
        self.p2 = p2
        self.grid = grid
        self.cp = p1 if cp is None else cp

        self.screen = screen
        self.screen.fill(WHITE)
        self.cell_size = cell_size
        self.w = 20 + grid.w*self.cell_size
        self.h = 20 + grid.h*self.cell_size

        self.grid_surf = pygame.Surface((self.w, self.h))
        self.text_surf = pygame.Surface((self.w,2*self.cell_size))
        self.text_surf.fill(WHITE)
        
        self.g = GridGraphicView(self.grid,p1,p2,self.grid_surf,self.cell_size,self)

    def run(self):
        self.draw()
        while not self.grid.is_over():

            c = -1
            while not self.grid.playable(c):
                c = self.g.get_col()

            self.g.anim_fall(c, self.cp)
            self.grid.play(c, self.cp)
            
            self.switch_player()
            save("save.p4", self)
            self.draw()
            
        w = self.grid.winner()
        if w is not None:
            print("Le gagnant est {} !".format(w))
        else:
            print("Match nul !")
            
    def switch_player(self):
        if self.cp == self.p1:
            self.cp = self.p2
        else:
            self.cp = self.p1

    def draw_player_turn(self):
        font = pygame.font.SysFont("monospace", 40)
        s = "{} joue ".format(self.cp)
        surf = font.render(s, True, BLACK, WHITE)
        tw, th = surf.get_size()

        new_surf = pygame.Surface((tw+th,th))
        new_surf.fill(WHITE)
        new_surf.blit(surf, (0,0))
        pygame.draw.circle(new_surf, self.g.p[self.cp],(tw+th//2,th//2),th//2-4)
        
        tw, th = new_surf.get_size()
        w,h = self.text_surf.get_size()
        surf = pygame.transform.scale(new_surf, (min(w,tw), min(h,th)))
        
        self.text_surf.blit(surf, ((w-min(w,tw))//2,(h-min(h,th))//2))
            
    def draw(self):
        self.g.draw()
        self.draw_player_turn()
        self.screen.blit(self.grid_surf, (0,0))
        self.screen.blit(self.text_surf, (0,self.h))
        pygame.display.flip()

    def update(self):
        self.screen.blit(self.grid_surf, (0,0))
        self.screen.blit(self.text_surf, (0,self.h))
        pygame.display.flip()
